import Comparator.ValueIntComparator;
import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import spout.Tweet;
import spout.TweetSpout;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Project: projects_cp
 * User: Gomes
 * Date: 09/12/13
 * Time: 15:21
 * <p/>
 * TODO: Class description
 */
public class UserTweetCount {


    public static class UserSplitTweet extends BaseBasicBolt {

        @Override
        public void execute(Tuple tuple, BasicOutputCollector collector) {
            Tweet tweet = (Tweet) tuple.getValue(0);
            String user = tweet.getUserID();
            collector.emit(new Values(user));
        }

        @Override
        public void declareOutputFields(OutputFieldsDeclarer declarer) {
            declarer.declare(new Fields("user"));
        }

        @Override
        public Map<String, Object> getComponentConfiguration() {
            return null;
        }
    }

    public static class UserCount extends BaseBasicBolt {
        Map<String, Integer> counts = new HashMap<String, Integer>();

        @Override
        public void execute(Tuple tuple, BasicOutputCollector collector) {
            String word = tuple.getString(0);
            if (word.equals("\u001a")){
                finish = true;
            }else{
                Integer count = counts.get(word);
                if (count == null)
                    count = 0;
                count++;
                counts.put(word, count);
//            collector.emit(new Values(word, count));
            }
        }

        @Override
        public void declareOutputFields(OutputFieldsDeclarer declarer) {
//            declarer.declare(new Fields("char", "count"));
        }

        public void cleanup(){
            try {
                ValueIntComparator vc =  new ValueIntComparator(counts);
                TreeMap<String,Integer> sorted_map = new TreeMap<String,Integer>(vc);
                PrintWriter pos = new PrintWriter("userCount.txt");
                sorted_map.putAll(counts);
                for(String s : sorted_map.keySet()){
                    pos.println(s + "\t" + counts.get(s));
                }
                pos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }
    }

    public static volatile boolean finish = false;

    public static void main(String[] args) throws Exception {

//        DataExtractor de = new DataExtractor("data/split10000_data_1.txt");

        TopologyBuilder builder = new TopologyBuilder();

        builder.setSpout("spout", new TweetSpout(), 1);

        builder.setBolt("user", new UserSplitTweet(), 8).shuffleGrouping("spout");
        builder.setBolt("count", new UserCount(), 1).fieldsGrouping("user", new Fields("user"));

        Config conf = new Config();
        conf.put("inputFile",args[0]);
//        conf.put("inputFile","data/all.txt");
//        conf.put("inputFile","data/checkin_data_4.txt");
//        conf.put("inputFile","data/split10000_data_1.txt");
//        conf.put("inputFile","data/split10000_data_1.txt");
        conf.setDebug(true);
//        conf.setDebug(false);

        conf.setMaxTaskParallelism(8);

        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("user-tweet-count", conf, builder.createTopology());

        while (true){
            if(finish){
                System.out.println("############# Kill #################");
                cluster.killTopology("user-tweet-count");

                Thread.sleep(1000);
                System.out.println("############# Shutdown #################");
                cluster.shutdown();
                break;
            }else
                Thread.sleep(1000);
        }

        System.out.println("Done Execution.");

    }

}
