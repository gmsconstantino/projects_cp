package Comparator;

import java.util.Comparator;
import java.util.Map;

/**
 * Project: projects_cp
 * User: Gomes
 * Date: 09/12/13
 * Time: 15:33
 * <p/>
 * TODO: Class description
 */
public class ValueIntComparator implements Comparator<String> {

    Map<String, Integer> base;
    public ValueIntComparator(Map<String, Integer> base) {
        this.base = base;
    }

    // Note: this comparator imposes orderings that are inconsistent with equals.
    public int compare(String a, String b) {
        if (base.get(a) >= base.get(b)) {
            return -1;
        } else {
            return 1;
        } // returning 0 would merge keys
    }

}
