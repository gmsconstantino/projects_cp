import Comparator.ValueIntComparator;
import Utils.MonthYear;
import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import spout.Tweet;
import spout.TweetSpout;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This topology demonstrates Storm's stream groupings and multilang capabilities.
 */
public class HashCountTopology {

    public static class HaveHashTweet extends BaseBasicBolt {
        Pattern p;

        @Override
        public void prepare(Map conf, TopologyContext context) {
            p = Pattern.compile(".*#.*");
        }

        @Override
        public void execute(Tuple tuple, BasicOutputCollector collector) {
            Tweet tweet = (Tweet) tuple.getValue(0);
            if(tweet.getTweet().equals("\u001a"))
                collector.emit(new Values(tweet));

            Matcher m = p.matcher(tweet.getTweet());
            if(m.matches())
                collector.emit(new Values(tweet));
        }

        @Override
        public void declareOutputFields(OutputFieldsDeclarer declarer) {
            declarer.declare(new Fields("tweet"));
        }

        @Override
        public Map<String, Object> getComponentConfiguration() {
            return null;
        }
    }


    public static class SplitByMonth extends BaseBasicBolt {

        @Override
        public void execute(Tuple tuple, BasicOutputCollector collector) {
            Tweet tweet = (Tweet) tuple.getValue(0);
            Calendar cal = Calendar.getInstance();
            Date date = tweet.getDate();
            cal.setTime(date);
            int month = cal.get(Calendar.MONTH) + 1;
            int year = cal.get(Calendar.YEAR);
            collector.emit(new Values(tweet,year,month));
        }

        @Override
        public void declareOutputFields(OutputFieldsDeclarer declarer) {
            declarer.declare(new Fields("tweet","year","month"));
        }

        @Override
        public Map<String, Object> getComponentConfiguration() {
            return null;
        }
    }

    public static class SplitSentenceTweet extends BaseBasicBolt {

        @Override
        public void execute(Tuple tuple, BasicOutputCollector collector) {
            Tweet tweet = (Tweet) tuple.getValue(0);
            String phrase = tweet.getTweet();
            for(String word : phrase.split(" "))
                if (word.length()>0)
                    collector.emit(new Values(word, tuple.getInteger(1),tuple.getInteger(2)));
        }

        @Override
        public void declareOutputFields(OutputFieldsDeclarer declarer) {
            declarer.declare(new Fields("word","year","month"));
        }

        @Override
        public Map<String, Object> getComponentConfiguration() {
            return null;
        }
    }

    public static class HashWordSelector extends BaseBasicBolt {

        @Override
        public void execute(Tuple tuple, BasicOutputCollector collector) {
            String word = tuple.getString(0);
            if(word.equals("\u001a")){
                collector.emit(new Values("\u001a",0,0));
            } else if (word.charAt(0) == '#'){
                String hashWord = word.substring(1);
                collector.emit(new Values(hashWord,tuple.getInteger(1),tuple.getInteger(2)));
            }
        }

        @Override
        public void declareOutputFields(OutputFieldsDeclarer declarer) {
            declarer.declare(new Fields("multipleHash","year","month"));
        }

    }

    public static class MultipleHashWordSelector extends BaseBasicBolt {

        @Override
        public void execute(Tuple tuple, BasicOutputCollector collector) {
            String word = tuple.getString(0);
            String[] split = word.split("#");
            for (String hash : split){
                collector.emit(new Values(hash.toLowerCase(), tuple.getInteger(1), tuple.getInteger(2)));
//                collector.emit(new Values(word));
            }
        }

        @Override
        public void declareOutputFields(OutputFieldsDeclarer declarer) {
            declarer.declare(new Fields("hash","year","month"));
        }
    }

    public static class RemovePonctuation extends BaseBasicBolt {

        Pattern p;

        @Override
        public void prepare(Map conf, TopologyContext context) {
            p = Pattern.compile("[^A-Za-z0-9']");
        }

        @Override
        public void execute(Tuple tuple, BasicOutputCollector collector) {
            String word = tuple.getString(0);
            if(word.equals("\u001a"))
                collector.emit(new Values("\u001a",0,0));

            Matcher m = p.matcher(word);
            word = m.replaceAll("");
            if(word.length()>0)
                collector.emit(new Values(word, tuple.getInteger(1), tuple.getInteger(2)));
        }

        @Override
        public void declareOutputFields(OutputFieldsDeclarer declarer) {
            declarer.declare(new Fields("word","year","month"));
        }
    }

    public static class WordCount extends BaseBasicBolt {
        //        Map<String, Integer> counts = new HashMap<String, Integer>();
        Map<MonthYear,Map<String, Integer>> byDate = new TreeMap<MonthYear, Map<String, Integer>>();

        @Override
        public void execute(Tuple tuple, BasicOutputCollector collector) {
            String word = tuple.getString(0);
            if (word.equals("\u001a")){
                finish = true;
            }else{
                MonthYear my = new MonthYear(tuple.getInteger(1),tuple.getInteger(2));
                Map<String, Integer> counts = byDate.get(my);
                if (counts == null){
                    counts = new HashMap<String, Integer>();
                    byDate.put(my,counts);
                }

                Integer count = counts.get(word);
                if (count == null)
                    count = 0;
                count++;
                counts.put(word, count);
//            collector.emit(new Values(word, count));
            }
        }

        @Override
        public void declareOutputFields(OutputFieldsDeclarer declarer) {
//            declarer.declare(new Fields("char", "count"));
        }

        public void cleanup(){
            try {
                PrintWriter pos = new PrintWriter("hash.txt");
                for(MonthYear my : byDate.keySet()){
                    Map<String, Integer> counts = byDate.get(my);
                    pos.print(my+"\t");
                    printNkeyword(counts,pos);
                    pos.println();
                }
                pos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        private void printNkeyword(Map<String,Integer> counts, PrintWriter pos){
            ValueIntComparator vc =  new ValueIntComparator(counts);
            TreeMap<String,Integer> sorted_map = new TreeMap<String,Integer>(vc);
            sorted_map.putAll(counts);
            int i = 0;
            int max = Math.min(sorted_map.keySet().size(),5);
            for(String s : sorted_map.keySet()){
                if (i<max-1){
                    pos.print(s + "("+ counts.get(s) +"),");
                    i++;
                }else if(i==max-1){
                    pos.print(s + "("+ counts.get(s) +")");
                    i++;
                } else break;
            }
        }

    }

    public static volatile boolean finish = false;

    public static void main(String[] args) throws Exception {

//        DataExtractor de = new DataExtractor("data/split10000_data_1.txt");

        TopologyBuilder builder = new TopologyBuilder();

//        builder.setSpout("spout", new RandomSentenceSpout(), 5);
//        builder.setSpout("spout", new RabbitTweetSpout(), 1);
        builder.setSpout("spout", new TweetSpout(), 1);

        builder.setBolt("filter", new HaveHashTweet(), 4).shuffleGrouping("spout");
        builder.setBolt("date", new SplitByMonth(), 4).shuffleGrouping("filter");
        builder.setBolt("split", new SplitSentenceTweet(), 4).shuffleGrouping("date");
        builder.setBolt("multiplehash", new HashWordSelector(), 4).shuffleGrouping("split");
        builder.setBolt("hash", new MultipleHashWordSelector(), 4).shuffleGrouping("multiplehash");
        builder.setBolt("word", new RemovePonctuation(), 4).shuffleGrouping("hash");
        builder.setBolt("count", new WordCount(), 1).fieldsGrouping("word", new Fields("word"));

        Config conf = new Config();
        conf.put("inputFile",args[0]);
//        conf.put("inputFile","data/all.txt");
//        conf.put("inputFile","data/checkin_data_4.txt");
//        conf.put("inputFile","data/split100000_data_1.txt");
//        conf.put("inputFile","data/split10000_data_1.txt");
        conf.setDebug(true);
//        conf.setDebug(false);

        conf.setMaxTaskParallelism(8);

        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("word-count", conf, builder.createTopology());

//        Thread.sleep(20000);
        while (true){
            if(finish){
                System.out.println("############# Kill #################");
                cluster.killTopology("word-count");

                Thread.sleep(1000);
                System.out.println("############# Shutdown #################");
                cluster.shutdown();
                break;
            }else
                Thread.sleep(1000);
        }

        System.out.println("Done Execution.");

    }
}
