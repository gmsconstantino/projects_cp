import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Gomes
 * Date: 02/12/13
 * Time: 22:37
 * To change this template use File | Settings | File Templates.
 */
public class TestMain {

    private static String[] lat = new String[]{
            "38.853679",
            "37.786681",
            "-6.184332",
            "37.764254",
            "-34.912106"};

    private static String[] lng = new String[]{
            "-77.043428",
            "-122.404394",
            "106.820759",
            "126.827694",
            "-56.159757"};

    public static void main(String[] args) {
            final Geocoder geocoder = new Geocoder();

//            GeocoderRequest geocoderRequest = new GeocoderRequestBuilder().setAddress("Rijnsburgstraat 9-11, Amsterdam, The Netherlands").getGeocoderRequest();

        for (int i = 0; i < lat.length; i++) {
            System.out.println("Test "+i);
            GeocoderRequest geocoderRequest = new GeocoderRequestBuilder().setLocation(new LatLng(lat[i],lng[i])).getGeocoderRequest();
            GeocodeResponse geocoderResponse = geocoder.geocode(geocoderRequest);
            List<GeocoderResult> results = geocoderResponse.getResults();
            String shortName = null;
            for (GeocoderResult g : results){
//                System.out.println(g.toString());
                    for(GeocoderAddressComponent address : g.getAddressComponents()){
                        if(address.getTypes().contains("country")) {
                            shortName = address.getShortName();
                            System.out.println(shortName);
                        }
                    }

            }
            System.out.println(LocationUtils.getContinentName(LocationUtils.getContinentCode(shortName)));
        }

        // Map to Continent

//            float latitude = results.get(0).getGeometry().getLocation().getLat().floatValue();
//            float longitude = results.get(0).getGeometry().getLocation().getLng().floatValue();
//        System.out.println("Lat "+latitude+" Long "+longitude);
//            assertEquals(52.347797f, latitude);
//            assertEquals(4.8507648f, longitude);
        }



   }
