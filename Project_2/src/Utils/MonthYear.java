package Utils;

/**
 * Project: projects_cp
 * User: Gomes
 * Date: 11/12/13
 * Time: 11:35
 */


public class MonthYear implements Comparable {

    private int month;
    private int year;

    public MonthYear(int year, int month) {
        this.month = month;
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MonthYear)) return false;

        MonthYear monthYear = (MonthYear) o;

        if (month != monthYear.month) return false;
        if (year != monthYear.year) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = month;
        result = 31 * result + year;
        return result;
    }

    @Override
    public int compareTo(Object o) {
        if (this == o) return 0;
        MonthYear monthYear = (MonthYear) o;

        return (year - monthYear.getYear())+(month - monthYear.getMonth());
    }

    @Override
    public String toString(){
        return year+"\t"+month;
    }

}
