package spout;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;

import java.io.FileNotFoundException;
import java.util.Date;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Gomes
 * Date: 03/12/13
 * Time: 11:19
 * To change this template use File | Settings | File Templates.
 */
public class TweetSpout extends BaseRichSpout {
    private SpoutOutputCollector collector;
    private Map config;
    private DataExtractor data;

    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        this.collector = collector;
        this.config = conf;
        try {
            data = new DataExtractor((String) config.get("inputFile"));
        } catch (FileNotFoundException e) {
            System.out.println(e.toString());
        }
    }

    @Override
    public void nextTuple() {
        Tweet t;

        if (!data.hasNextTweet()){
            try {
                Thread.sleep(1000);
                collector.emit(new Values(new Tweet("\u001a",0.0,0.0,"\u001A",new Date(),"")));
            } catch (InterruptedException e) {
                return;
            }
            return;
        }
        t = data.readTweet();
        if (t == null) return;
        collector.emit(new Values(t));
    }

    @Override
    public void ack(Object id) {
    }

    @Override
    public void fail(Object id) {
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("tweet"));
    }


}
