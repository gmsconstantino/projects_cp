package spout;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import com.rabbitmq.client.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Gomes
 * Date: 03/12/13
 * Time: 11:19
 * To change this template use File | Settings | File Templates.
 */
public class RabbitTweetSpout extends BaseRichSpout {

    private SpoutOutputCollector collector;
    private Map config;
    Channel channel;
    QueueingConsumer consumer;

    private static final String QUEUE_NAME = "cp-queue";

    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        this.collector = collector;
        this.config = conf;

        try{
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");
            Connection connection = factory.newConnection();
            channel = connection.createChannel();
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);

//            consumer = new QueueingConsumer(channel);
//            channel.basicConsume(QUEUE_NAME, true, consumer);
        } catch (IOException e) {
            return;
        }
    }

    @Override
    public void nextTuple() {
        ByteArrayInputStream bis = null;
        ObjectInput in = null;
        try {
            int i = 0;
            Tweet t;
            while (i < 1000){
//                QueueingConsumer.Delivery delivery = consumer.nextDelivery();
                boolean autoAck = true;
                GetResponse response = channel.basicGet(QUEUE_NAME, autoAck);
                if (response == null) {
                    // No message retrieved.
                    Thread.sleep(200);
                } else {
//                    byte[] body = response.getBody();
                    bis = new ByteArrayInputStream(response.getBody());
                    in = new ObjectInputStream(bis);
                    t = (Tweet) in.readObject();
                    // TODO; remove getTweet o emit the object tweet
                    collector.emit(new Values(t.getTweet()));
                }
                i++;
            }
        } catch (InterruptedException e) {
            return;
        } catch (IOException e) {
            return;
        } catch (ClassNotFoundException e) {
            return;
        } finally {
            try {
                bis.close();
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
            // ignore close exception
            }
        }
    }

    @Override
    public void deactivate(){
        try {
            System.out.println("Deactivate Spout");
            channel.abort();
            channel.close();
        } catch (IOException e) {
            return;
        }
    }

    @Override
    public void ack(Object id) {
    }

    @Override
    public void fail(Object id) {
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("tweet"));
    }


}
