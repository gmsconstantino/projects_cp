package spout;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.NoSuchElementException;

public class DataExtractor {

	private File file;
	private BufferedReader scanner;
    private boolean endOfFile;

	public DataExtractor(String filePath) throws FileNotFoundException {
		this.file = new File(filePath);
		this.scanner = new BufferedReader(new FileReader(this.file));
        endOfFile = false;
	}

    public boolean hasNextTweet(){
        return !endOfFile;
    }

	/**
	 * Example tweet (UserID\tTweetID\tLatitude\tLongitude\tCreatedAt\tText\tPlaceID):
	 * 41418752	19395304894	38.929854	-77.027976	2010-07-24 04:25:41	I'm at The Wonderland Ballroom (1101 Kenyon St NW, at 11th St, Washington, DC) w/ 2 others. http://4sq.com/2z5p82	8dc80c56f429dd1e
	 */
	public Tweet readTweet() {
        String line = null;
        try {
            line = scanner.readLine();
            if (line != null) { // You might also want to check for empty?
                Tweet tweet = this.processLine(line);
                if(tweet!=null)
                    return tweet;
                else
                    return readTweet();
            }
            endOfFile = true;
            return null;
        } catch (IOException e) {
            return null;
        }
	}

	private Tweet processLine(String checkin){
		try{
			String[] scanner = checkin.split("\t");
            String userID = scanner[0];
			String tweetID = scanner[1];
			double latitude = new Double(scanner[2]);
			double longitude = new Double(scanner[3]);
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = formatter.parse(scanner[4]);
			String text = scanner[5];
	//		String placeID = scanner.next();
			return new Tweet(text, latitude, longitude, userID, date, tweetID);
		}catch(NoSuchElementException e){
//			System.err.println("Error reading: "+checkin);
			return null;
		}catch (ArrayIndexOutOfBoundsException e){
            return null;
        }catch (ParseException e){
            return null;
        }
	}

	public int countWords(Tweet tweet){
//		System.out.println("Tweet: "+tweet.getTweet());
		String[] words = tweet.getTweet().split(" ");
//		System.out.println("\tSize:"+words.length);
		return words.length;
	}


	public static void main(String[] args) {
		try {
			DataExtractor de = new DataExtractor("data/split10000_data_1.txt");
            long stime = System.currentTimeMillis();
            Tweet t = de.readTweet();
			int total = 0;
			int progress = 0;
			int count;
			while(t!=null){
				count = de.countWords(t);
				total+=count;
				progress+=count;
				t = de.readTweet();
				if(progress>=100000){
					System.out.println("Lastest count: "+total);
					progress=0;
				}
			}
            long ftime = System.currentTimeMillis();
			System.out.println("Counting finished:"+total);
            System.out.println("Time "+(ftime - stime));

        } catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
