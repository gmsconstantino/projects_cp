import Utils.Coordenates;
import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import spout.Tweet;
import spout.TweetSpout;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class TracingUserTopology {


    public static class SplitCoordTweet extends BaseBasicBolt {
    	Map conf;

    	@Override
    	public void prepare(Map conf, TopologyContext context){
    		this.conf = conf;
    	}

        @Override
        public void execute(Tuple tuple, BasicOutputCollector collector) {
            Tweet tweet = (Tweet) tuple.getValue(0);
            String user = tweet.getUserID();
            Double lat = tweet.getLatitude();
            Double lon = tweet.getLongitude();
            if(user.equals(conf.get("userId")) || user.equals("\u001a")){
            	Coordenates coord = new Coordenates(lat, lon);
            	collector.emit(new Values(coord, user));
            }
        }

        @Override
        public void declareOutputFields(OutputFieldsDeclarer declarer) {
            declarer.declare(new Fields("coord","user"));
        }

    }

    public static class CoordCountTweet extends BaseBasicBolt {
    	Map conf;
    	Map<Coordenates,Integer> coordCount = new HashMap<Coordenates, Integer>();
    	
    	@Override
    	public void prepare(Map conf, TopologyContext context){
    		this.conf = conf;
    	}

        @Override
        public void execute(Tuple tuple, BasicOutputCollector collector) {
            Coordenates coord = (Coordenates) tuple.getValue(0);
            String user = tuple.getString(1);
            if(user.equals("\u001a")){
                finish = true;
            }else{
                Integer count = coordCount.get(coord);
                if (count == null)
                    count = 0;
                count++;
                coordCount.put(coord, count);

            }
        }

        @Override
        public void declareOutputFields(OutputFieldsDeclarer declarer) {
        }

        public void cleanup(){
            try {
                PrintWriter pos = new PrintWriter("coordByUser-"+conf.get("userId")+".txt");
                PrintWriter pos2 = new PrintWriter("coordByUserCount-"+conf.get("userId")+".txt");
                for(Coordenates c : coordCount.keySet()){
                    pos.println(c);
                    pos2.println(c + "\t" + coordCount.get(c));
                }
                pos.close();
                pos2.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }
    }

    public static volatile boolean finish = false;

    public static void main(String[] args) throws Exception {

//        DataExtractor de = new DataExtractor("data/split10000_data_1.txt");

        TopologyBuilder builder = new TopologyBuilder();

        builder.setSpout("spout", new TweetSpout(), 1);
        builder.setBolt("coord", new SplitCoordTweet(), 8).shuffleGrouping("spout");
        builder.setBolt("count", new CoordCountTweet(), 1).fieldsGrouping("coord", new Fields("coord"));

        Config conf = new Config();
        conf.put("inputFile",args[0]);
//        conf.put("inputFile","data/checkin_data_4.txt");
        conf.put("userId",args[1]);
//        conf.put("inputFile","data/split10000_data_1.txt");
//        conf.put("inputFile","data/split10000_data_1.txt");
        conf.setDebug(true);
//        conf.setDebug(false);

        conf.setMaxTaskParallelism(8);

        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("user-tweet-count", conf, builder.createTopology());

        while (true){
            if(finish){
                System.out.println("############# Kill #################");
                cluster.killTopology("user-tweet-count");

                Thread.sleep(1000);
                System.out.println("############# Shutdown #################");
                cluster.shutdown();
                break;
            }else
                Thread.sleep(1000);
        }

        System.out.println("Done Execution.");

    }

}
 