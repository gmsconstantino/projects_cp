import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import spout.DataExtractor;
import spout.Tweet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

/**
 * Project: Projecto 2 CP
 * User: Gomes
 * Date: 06/12/13
 * Time: 11:50
 *
 * TODO: Description Class
 */
public class RabbitDataExtractor {

    private static final String QUEUE_NAME = "cp-queue";

    public static void main(String[] args) throws IOException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        DataExtractor de = new DataExtractor("data/split10000_data_1.txt");

        ByteArrayOutputStream bos = null;
        ObjectOutput out = null;
        try {
            long stime = System.currentTimeMillis();
            Tweet t = de.readTweet();
            byte[] tweetBytes;
            while(t!=null){
                bos = new ByteArrayOutputStream();
                out = new ObjectOutputStream(bos);
                out.writeObject(t);
                tweetBytes = bos.toByteArray();
                channel.basicPublish("", QUEUE_NAME, null, tweetBytes);
                t = de.readTweet();
            }
            long ftime = System.currentTimeMillis();
            System.out.println("Time "+(ftime - stime));
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException ex) {
                // ignore close exception
            }
            try {
                bos.close();
            } catch (IOException ex) {
                // ignore close exception
            }
        }

        channel.close();
        connection.close();
    }

}
