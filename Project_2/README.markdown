

## Running project with Maven

The project contains (pom.xml) which can be used with Maven using the `-f` option. For example, to
compile and run the topologies in local mode, use the command:

    $ mvn compile exec:java -Dexec.classpathScope=compile -Dexec.mainClass=TracingUserTopology -Dexec.args="userId"
    $ mvn compile exec:java -Dexec.classpathScope=compile -Dexec.mainClass=UserTweetCount -Dexec.args="data/checkin_data_4.txt"
    $ mvn compile exec:java -Dexec.classpathScope=compile -Dexec.mainClass=WordCountTopology -Dexec.args="data/checkin_data_4.txt"

Debug Maven

$ export MAVEN_OPTS="-Xdebug -Xnoagent -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=8000";
